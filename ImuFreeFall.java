/*
    Imu: a server for Wireless IMU
    Copyright (C) 2016 giovanni.organtini@roma1.infn.it
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

public class ImuFreeFall {

    public static void main(String[] args) {
	FreeFall server = new FreeFall();
	server.setZSensor(3);
	AbstractObserver bobs = new AccelerationYObserver("Acceleration y", 1.);
	server.addObserver(bobs);
	server.start();
    }

    public static void license() {
        System.out.println("\n\n\n");
        System.out.println("Imu Server Copyright (C) 2016 giovanni.organtini@roma1.infn.it\n" +
                           "This program comes with ABSOLUTELY NO WARRANTY;\n" +
                           "This is free software, and you are welcome to redistribute it\n" +
                           "under certain conditions; visit http://www.gnu.org/licenses/gpl-3.0.en.html\n" +
                           "for details.\n");
    }
}
