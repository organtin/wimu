GLIB=WImu/jfreechart/lib/jfreechart-1.0.17.jar:WImu/jfreechart/lib/jcommon-1.0.21.jar
LIB=$(GLIB):WImu/WImu.jar

run: Imu.class ImuFreeFall.class
	java -cp .:$(LIB) Imu

clean:
	rm -rf *~
	rm -rf *.class
	cd WImu && $(MAKE) clean

lib: 
	cd WImu && $(MAKE)

Imu.class: Imu.java lib
	javac -cp WImu/WImu.jar Imu.java 

ImuFreeFall.class: ImuFreeFall.java lib
	javac -cp WImu/WImu.jar ImuFreeFall.java 


Imu: run

ImuFreeFall: ImuFreeFall.class
	java -cp .:$(LIB) ImuFreeFall

