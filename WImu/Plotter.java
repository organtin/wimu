import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import org.jfree.chart.*;
import org.jfree.chart.plot.*;
import org.jfree.data.*;
import org.jfree.data.general.*;
import org.jfree.data.xy.*;

/*
    WImu: a java library for Wireless IMU
    Copyright (C) 2016 giovanni.organtini@roma1.infn.it                                                        
 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

public class Plotter extends JPanel {

    XYSeries _data;
    ChartPanel _chartPanel;
    
    int _xc;
    int _yc;

    public Plotter(String title, String xlabel, String ylabel) {
	_data = new XYSeries(title);
	XYSeriesCollection coll = new XYSeriesCollection(_data);
	JFreeChart chart = ChartFactory.createXYLineChart(title,
							  xlabel, 
							  ylabel,
							  coll,
							  PlotOrientation.VERTICAL,
							  false,
							  true,
							  false
							  );
	_chartPanel = new ChartPanel(chart);
	// setup the size of the panel
	this.setLayout(new GridLayout(1, 1));
	this.add(new ChartPanel(chart),  BorderLayout.CENTER);
    }

    public void set(double x, double y) {
	try {
	    _data.add(x, y);
	}
	catch (Exception e) {
	    // if out of memory exception then cut _data
	    _data.clear();
	}
    }

    public void plot() {
	if (_data.getItemCount() > 0) {
	    setVisible(true);
	}
    }

}
