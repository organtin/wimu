/*
    WImu: a java library for Wireless IMU
    Copyright (C) 2016 giovanni.organtini@roma1.infn.it                                                        
 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

public class FreeFall extends ImuServer {
    //
    // This class show the acceleration until the sensor identified by index _zSensor in the
    // data structure goes from about zero to about 9.8
    //
    int _zSensor;
    boolean _isFalling;
    int count;

    FreeFall() {
	super();
	_isFalling = false;
	_zSensor = -1;
    }

    FreeFall(int zSensor) {
	super();
	_isFalling = false;
	_zSensor = zSensor;
    } 

    public void setZSensor(int s) {
	_zSensor = s;
    }

    public int zSensor() {
	return _zSensor;
    }

    public boolean stillLooping(String[] data) {
	boolean ret = true;
	if ((_isFalling) && (Double.parseDouble(data[_zSensor]) > 9)) {
	    // stop looping if no more falling (but wait some time)
	    count++;
	    if (count > 20) {
		ret = false;
	    } else if (count == 1) {
		System.out.println("-----> Stop falling!");
	    }
	} else if (Double.parseDouble(data[_zSensor]) < 4) {
	    _isFalling = true;
	    count = 0;
	    System.out.println("-----> Falling...");
	}
	return ret;
    }
}
