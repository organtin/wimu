import java.text.*;

/*
    WImu: a java library for Wireless IMU
    Copyright (C) 2016 giovanni.organtini@roma1.infn.it                                                        
 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

public class AccelerationObserver extends AbstractObserver {
    
    Double[] _lastx;
    Double[] _lastv;
    Double[] _average;
    
    int _nAverage;

    DecimalFormat _df;

    AccelerationObserver(String title, double screenFraction) {
	super(title, "t [s]", "a [ms^-2]", "Accelerometer", screenFraction);
	init();
    }

    AccelerationObserver(String title) {
	super(title, "t [s]", "a [ms^-2]", "Accelerometer");
	init();
    }

    private void init() {
	_lastx = new Double[3];	
	_lastv = new Double[3];
	_average = new Double[3];
	//evaluateInitialState(10);
	for (int i = 0; i < 3; i++) {
	    _lastx[i] = 0.;
	    _lastv[i] = 0.;
	    _average[i] = 0.;
	}
	_nAverage = 0;
	_df = new DecimalFormat("0.00");       
	setOutputFileName("a.dat");
    }

    public void process(String sensor, Double t, Double dt, 
			Vector a, Vector v, Vector x)
    {
	Double[] toStore = new Double[11];
	toStore[0] = t;
	toStore[1] = dt;
	for (int i = 0; i < 3; i++) {
	    toStore[i + 2] = a.x(i); 
	    toStore[i + 5] = v.x(i);
	    toStore[i + 8] = x.x(i);
	}
	store(toStore);
    }
}
