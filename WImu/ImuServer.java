import java.io.*;
import java.net.*;
import java.util.*;

import java.awt.*;

/*
    WImu: a java library for Wireless IMU
    Copyright (C) 2016 giovanni.organtini@roma1.infn.it                                                        
 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

public class ImuServer {
    DatagramSocket _ds;
    Double _t0;
    Double _tlast;
    boolean _debug;
    java.util.List<AbstractObserver> _observers;
    
    static String[] _sensors = {"Accelerometer", "Gyroscope", "Magnetometer"};

    ImuServer() {
	_debug = false;
	_t0 = -1.;
	_tlast = 0.;
	_observers = new ArrayList<AbstractObserver>();
	try {
	    _ds = new DatagramSocket(5555);
	}
	catch (Exception e) {
	    System.out.println(e.toString());
	} 
    }

    public void setDebug() {
	_debug = true;
    }

    public void unsetDebug() {
	_debug = false;
    }

    public boolean debug() {
	return _debug;
    }

    public void addObserver(AbstractObserver o) {
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	int n = _observers.size();
	int x = n * (int)screenSize.getWidth()/2;
	int y = 0;
	if (n > 1) {
	    x = 0;
	    y = (int)screenSize.getHeight()/2;;
	}
	o.setLocation(x, y);
	_observers.add(o);
    }
    
    public void start() {
	boolean started = false;
	boolean loop = true;
	try {
	    byte[] buf = new byte[256];
	    DatagramPacket pkt = new DatagramPacket(buf, buf.length);	    
	    while (loop) {
		_ds.receive(pkt);
		if (!started) {
		    InetAddress addr = pkt.getAddress();
		    System.out.println("Connected from " + addr);
		    started = true;
		}
		String rawdata = new String(buf);
		String[] data = rawdata.split(", *");
		analyze(data);
		loop = stillLooping(data);
	    }
	}
	catch (Exception e) {
	    System.out.println("Whops! " + e.toString());
	}
    }

    public void analyze(String[] data) {
	//
	// data are expected in the following form
	// time
	// sensor id
	// x, y, z of the sensor
	//
	// sensors are 3 = acc, 4 = gyro, 5 = mag
	//
	if (debug()) {
	    dumpData(data);
	}
	Double t = Double.parseDouble(data[0]);	
	// set the first timestamp as the time origin
	if (_t0 < 0) {
	    _t0 = t;
	}
	t -= _t0;
	// compute the time interval
	Double dt = t - _tlast;
	_tlast = t;
	// loop on sensors
	int sensors = (data.length - 1)/4;
	int j = 1;
	for (int i = 0; i < sensors; i++) {
	    int sensorId = Integer.parseInt(data[j++]);
	    Double x = Double.parseDouble(data[j++]);
	    Double y = Double.parseDouble(data[j++]);
	    Double z = Double.parseDouble(data[j++]);
	    for (AbstractObserver o: _observers) {
		o.notify(_sensors[sensorId - 3], t, dt, x, y, z);
	    }
	}	
    }
    
    public void dumpData(String[] data) {
	//
	// dump data received
	//
	for (int i = 0; i < data.length; i++) {
	    int k = (i % 4) + 1;
	    if (k == 2) {
		if (data[i].equals("3")) {
		    System.out.print("a:");
		} else if (data[i].equals("4")) {
		    System.out.print("g:");
		} else if (data[i].equals("5")) {
		    System.out.print("B:");
		}
	    } else {
		System.out.print(data[i]);
	    }
	    System.out.print(" ");
	}
	System.out.println();
    }

    public boolean stillLooping(String[] data) {
	//
	// returns false if need not to continue
	//
	return true;
    }
}  

