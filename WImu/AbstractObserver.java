import java.io.*;
import java.text.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/*
    WImu: a java library for Wireless IMU
    Copyright (C) 2016 giovanni.organtini@roma1.infn.it                                                        
 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

public abstract class AbstractObserver {

    Plotter _plotter;
    BufferedWriter _out;

    Vector _a0;
    Vector _a02;
    Vector _rms;

    Vector _x;
    Vector _v;

    Integer  _n;
    Integer  _nmax;

    boolean _zero;

    String _sensor;
    Double _t0;

    JFrame _frame;

    AbstractObserver(String title, String xlabel, String ylabel,
		     String observedSensor) {
	init(title, xlabel, ylabel, observedSensor, 0.45);
    }

    AbstractObserver(String title, String xlabel, String ylabel,
		     String observedSensor, double screenFraction) {
	init(title, xlabel, ylabel, observedSensor, screenFraction);
    }

    private void init(String title, String xlabel, String ylabel,
		      String observedSensor, double screenFraction) {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	_frame = new JFrame( "IMU Server" );
	_plotter = new Plotter(title, xlabel, ylabel);
	_frame.getContentPane().add(_plotter, BorderLayout.CENTER);
	Double w = screenSize.getWidth() * screenFraction;
	Double h = screenSize.getHeight() * screenFraction;
	_frame.setSize(w.intValue(), h.intValue());
	_frame.setVisible(true);
	_n    = 0;
	_nmax = 0;
	_sensor = observedSensor;
	_zero = false;
	_a0 = new Vector();
	_a02 = new Vector();
	_rms = new Vector();
	_x = new Vector();
	_v = new Vector();
    }

    public void setLocation(int x, int y) {
	_frame.setLocation(x, y);
    }

    public void showInitialValues() {
	DecimalFormat df = new DecimalFormat("0.00");
	for (int i = 0; i < 3; i++) {
	    System.out.println("x[" + i + "] = " + df.format(_a0.x(i)) + 
			       " +- " + df.format(_rms.x(i)));
	}
	System.out.println("|x| = " + df.format(_a0.size()));
    }

    public void evaluateInitialState(int n) {
	_zero = true;
	_nmax += n;
    }

    public void setOutputFileName(String file) {
	try {
	    FileWriter fstream = new FileWriter(file);
	    _out = new BufferedWriter(fstream);
	}
	catch (IOException e) {
	    System.out.println(e.toString());
	}
    }

    public void store(Double[] x) {
	try {
	    for (int i = 0; i < x.length; i++) {
		_out.write(x[i] + "\t");
	    }
	    _out.write("\n");	    
	    _out.flush();
	}
	catch (IOException e) {
	    System.out.println(e.toString());
	}
    }

    public void plot() {
	if (_plotter != null) {
	    _plotter.plot();
	}
    }

    public void set(double x, double y) {
	if (_plotter != null) {
	    _plotter.set(x, y);
	} 
    }

    public void notify(String sensor, Double t, Double dt, 
		       Double ax, Double ay, Double az) {
	if (_n < 1) {
	    _t0 = t;
	}
	t -= _t0;
	if (sensor.equals(_sensor)) {
	    Vector a = new Vector(ax, ay, az);
	    if (_zero) {
		if (_n < _nmax + 10) {
		    if (_n > 10) { // skip the first 10 measurements
			Vector a2 = new Vector(ax*ax, ay*ay, az*az);
			_a0.add(a);
			_a02.add(a2);
			System.out.print("Evaluating initial value: " + 
					 (_n - 10) + "\r");
		    }
		} else {
		    _n -= 10;
		    _a0.times(1./_n);
		    _a02.times(1./_n);
		    for (int i = 0; i < 3; i++) {
			_rms.set(i, Math.sqrt(_a02.x(i) - _a0.x(i)*_a0.x(i)));
			_zero = false;
		    }
		    System.out.println("");
		    showInitialValues();
		}
	    } else {
		//
		// subtract gravity if needed
		//
		a.subtract(_a0);
		process(sensor, t, dt, a, _v, _x);
	    }
	}
	_n++;
    }

    abstract void process(String sensor, Double t, Double dt,
			  Vector a, Vector v, Vector x);
}
