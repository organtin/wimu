import java.text.*;

/*
    WImu: a java library for Wireless IMU
    Copyright (C) 2016 giovanni.organtini@roma1.infn.it                                                        
 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


public class AccelerationZObserver extends AccelerationObserver {
    
    AccelerationZObserver(String title) {
	super(title);
    }

    public void process(String sensor, Double t, Double dt, 
			Vector a, Vector v, Vector x)
    {
	super.process(sensor, t, dt, a, v, x);
	if (t > 3) {
	    set(t, a.z()); 
	    plot();	    
	} 
    }
}
