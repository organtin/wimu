/*
    WImu: a java library for Wireless IMU
    Copyright (C) 2016 giovanni.organtini@roma1.infn.it                                                        
 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

public class Vector {
    Double[] _x;

    public Vector() {
	_x = new Double[3];
	for (int i = 0; i <3; i++) {
	    _x[i] = 0.;
	}
    }

    public Vector(Double x, Double y, Double z) {
	_x = new Double[3];
	_x[0] = x; 
	_x[1] = y;
	_x[2] = z;
    }

    public Vector(Vector right) {
	_x = new Double[3];
	for (int i = 0; i <3; i++) {
	    _x[i] = right._x[i];
	}
    }

    public void copy(Vector right) {
	for (int i = 0; i <3; i++) {
	    _x[i] = right._x[i];
	}	
    }

    public Double size() {
	Double m2 = 0.;
	for (int i = 0; i < 3; i++) {
	    m2 += _x[i]*_x[i];
	}
	return Math.sqrt(m2);
    }

    public Vector add(Vector right) {
	for (int i = 0; i < 3; i++) {
	    _x[i] += right._x[i];
	}
	return this;
    }

    public Vector subtract(Vector right) {
	for (int i = 0; i < 3; i++) {
	    _x[i] -= right._x[i];
	}
	return this;
    }

    public Vector times(Double x) {
	for (int i = 0; i < 3; i++) {
	    _x[i] *= x;
	}
	return this;
    }

    public Double x() {
	return _x[0];
    }

    public Double y() {
	return _x[1];
    }

    public Double z() {
	return _x[2];
    }

    public Double x(int i) {
	return _x[i];
    }

    public void set(int i, Double x) {
	_x[i] = x;
    }
}
